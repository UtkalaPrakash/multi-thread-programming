Documentation for Warmup Assignment 2
=====================================

+-------+
| BUILD |
+-------+

Comments: make will create a executable warmup2 file.
./warmup2 with required arguments run the script

+---------+
| GRADING |
+---------+

Basic running of the code : 100 out of 100 pts

Missing required section(s) in README file : No
Cannot compile : No
Compiler warnings : No
"make clean" : yes
Segmentation faults : No
Separate compilation : No
Using busy-wait : No
Handling of commandline arguments:
    1) -n : yes
    2) -lambda : yes
    3) -mu : yes
    4) -r : yes
    5) -B : yes
    6) -P : yes
Trace output :
    1) regular packets: 7 lines
    2) dropped packets: 1 line
    3) removed packets: yes
    4) token arrival (dropped or not dropped): 1 line
Statistics output :
    1) inter-arrival time : yes
    2) service time : yes
    3) number of customers in Q1 : yes
    4) number of customers in Q2 : yes
    5) number of customers at a server : yes
    6) time in system : yes
    7) standard deviation for time in system : yes
    8) drop probability : yes
Output bad format : No
Output wrong precision for statistics (should be 6-8 significant digits) : No
Large service time test : Yes
Large inter-arrival time test : yes
Tiny inter-arrival time test : yes
Tiny service time test : yes
Large total number of customers test : yes
Large total number of customers with high arrival rate test : yes
Dropped tokens test : yes
Cannot handle <Cntrl+C> at all (ignored or no statistics) : It handles
Can handle <Cntrl+C> but statistics way off : No prints statistics
Not using condition variables and do some kind of busy-wait : Using condition variable
Synchronization check : yes
Deadlocks : No

+----------------------+
| BUGS / TESTS TO SKIP |
+----------------------+

Is there are any tests in the standard test suite that you know that it's not
working and you don't want the grader to run it at all so you won't get extra
deductions, please list them here.  (Of course, if the grader won't run these
tests, you will not get plus points for them.)

Comments: Script is working for all tests

+------------------+
| OTHER (Optional) |
+------------------+

Comments on design decisions: ?
Comments on deviation from spec: ?

