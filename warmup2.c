#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "my402list.h"
#include "cs402.h"

// These are in msec
float lambda_inv = 1000, mu_inv = 2857.143, r_inv = 666.667;
int B = 10, P = 3, num = 20;

// Variables for Statistics
float totalServiceTime[2] = {0}, totalQ1time = 0, totalQ2time = 0, totalInterArrivalTime = 0, totalSysTime = 0, sqTotalSysTime = 0;
int token_num = 0, tokenCount = 0, droppedTokenCount = 0, droppedPackets = 0, removedPackets = 0, packet_count = 0;

sigset_t set;
My402List q1_list, q2_list; 
pthread_t Q1Thread, TokenBucketThread, Server1Thread, Server2Thread, CtrlThread;
pthread_mutex_t output_mutex = PTHREAD_MUTEX_INITIALIZER, mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t awake = PTHREAD_COND_INITIALIZER;
int q_complete = 0, readme = 0, sig;
struct timeval time_now;
long start_sec, start_usec;
typedef struct {
	int number;
	int num_tokens;
	float start;
	float arrival;
	float departure;
	float service_time;
} packets;
typedef struct {
	packets *packet;
	float arrival;
	float departure;
	float actual_service_time;
} framed_packets;

// Read file and ARG for data
// Statistics
/*------------------------------ Function Utilities ------------------------*/

static
float nowTime() {
    gettimeofday(&time_now, 0);
    float ret_sec = (float) (time_now.tv_sec - start_sec);
    float ret_msec = (float) (time_now.tv_usec - start_usec)/1000;
    return((ret_sec * 1000) + ret_msec);
}

static
void Output ( float timeNow, int type, int number, char *action, char *list_name, unsigned int token_count, float val1, float val2 ) {
    pthread_mutex_lock(&output_mutex);
    if ( type == 3 ) {
	printf("Input file %s %s", list_name, action);
    } else if ( type == 2 ) {
	printf("%011.3f ms: Exiting %s, ctrl+c signal found", timeNow, list_name);
    } else if ( type == 1 ) {
	if ( !strcmp(action, "arrive") ) {
	    printf("%011.3f ms: p%d arrives, needs %d tokens, inter-arrival time = %fms", timeNow, number, token_count, val1);
	} else if ( !strcmp(action, "enter") ) {
	    if ( !strcmp(list_name, "S1") || !strcmp(list_name, "S2") ) {
		printf("%011.3f ms: p%d begins service at %s, requesting %fms of service", timeNow, number, list_name, val1);
	    } else if ( !strcmp(list_name, "Q1") || !strcmp(list_name, "Q2") ) {
	        printf("%011.3f ms: p%d enters %s", timeNow, number, list_name);
	    } else {}
	} else if ( !strcmp(action, "leave") ) {
	    if ( !strcmp(list_name, "S1") || !strcmp(list_name, "S2") ) {
		printf("%011.3f ms: p%d departs from %s, service time = %fms, time in system = %fms", timeNow, number, list_name, val1, val2);
	    } else if ( !strcmp(list_name, "Q1") ) {
		printf("%011.3f ms: p%d leaves Q1, time in Q1 = %fms, token bucket now has %d token(s)", timeNow, number, val1, token_count);
	    } else if ( !strcmp(list_name, "Q2") ) {
		printf("%011.3f ms: p%d leaves Q2, time in Q2 = %fms", timeNow, number, val1);
	    } else {}
	} else if ( !strcmp(action, "remove") ) {
	    printf("%011.3f ms: p%d removed from %s",timeNow, number, list_name);
	} else {
	    printf("%011.3f ms: p%d arrives, needs %d tokens, inter-arrival time = %fms, dropped", timeNow, number, token_count, val1);
	}
    } else {
	if ( strcmp(action, "arrive") ) {
	    printf("%011.3f ms: token t%d arrives, dropped", timeNow, number);
	} else {
	    printf("%011.3f ms: token t%d arrives, token bucket now has %d token(s)", timeNow, number, token_count);
	}
    }
    printf("\n");
    pthread_mutex_unlock(&output_mutex);
}

int generateQ2() {
    int req_tokens = 0;
    int left_tokens = 0;

    pthread_mutex_lock(&mutex);

    // Read first element in Q1
    if ( q1_list.num_members == 0 ) {
	pthread_mutex_unlock(&mutex);
	return(0);
    }
    My402ListElem *firstQ1elem = My402ListFirst(&q1_list);
    packets *firstPacket = (packets *) firstQ1elem->obj;
    req_tokens = firstPacket->num_tokens;

    // Read token count
    if ( tokenCount >= req_tokens ) {
	tokenCount -= req_tokens;
	left_tokens = tokenCount;
    } else {
	pthread_mutex_unlock(&mutex);
	return (0);
    }

    // Remove from Q1 list
    My402ListUnlink(&q1_list, firstQ1elem);
    pthread_mutex_unlock(&mutex);
    firstPacket->departure = nowTime();
    Output(nowTime(), 1, firstPacket->number, "leave", "Q1", left_tokens, (firstPacket->departure - firstPacket->arrival), 0);

    // Write to Q2 list
    framed_packets *framed_packet_obj = malloc(sizeof(framed_packets));
    framed_packet_obj->packet = firstPacket;
    framed_packet_obj->arrival = nowTime();
    pthread_mutex_lock(&mutex);
    My402ListAppend(&q2_list, (void*)framed_packet_obj);
    pthread_mutex_unlock(&mutex);
    Output(framed_packet_obj->arrival, 1, firstPacket->number, "enter", "Q2", 0, 0, 0);

    // Signal server to transmit
    pthread_cond_signal(&awake);
    return(0);
}

static
void unlink_lists () {
    pthread_mutex_trylock(&mutex);
    removedPackets += q2_list.num_members;
    removedPackets += q1_list.num_members;
    int i;
    My402ListElem *elem = My402ListFirst(&q1_list);
    for ( i = 0; i<q1_list.num_members; i++ ) {
	packets *pkt = (packets *) elem->obj;
	Output(nowTime(), 1, pkt->number, "remove", "Q1", 0 ,0 ,0);
	elem = My402ListNext(&q1_list, elem);
    }
    elem = My402ListFirst(&q2_list);
    for ( i = 0; i<q2_list.num_members; i++ ) {
	framed_packets *framed_packet = (framed_packets *) elem->obj;
	Output(nowTime(), 1, framed_packet->packet->number, "remove", "Q2", 0 ,0 ,0);
	elem = My402ListNext(&q2_list, elem);
    }
    My402ListUnlinkAll(&q2_list);
    My402ListUnlinkAll(&q1_list);
    pthread_mutex_unlock(&mutex);
}

static
void statistics () {
    float emulation_time = nowTime();
    int completedPackets = packet_count - droppedPackets - removedPackets;
    float stdDev = sqrtf((sqTotalSysTime/completedPackets) - ((totalSysTime/completedPackets)*(totalSysTime/completedPackets)))/1000;
    if ( stdDev != stdDev ) stdDev = 0;
    printf("\nStatistics:\n\n");
    if ( packet_count ) {
    	printf("\taverage packet inter-arrival time = %.6g sec\n",(float) totalInterArrivalTime/(packet_count * 1000));
    	printf("\taverage packet service time = %.6g sec\n\n", (float) (totalServiceTime[0] + totalServiceTime[1])/(packet_count*1000));
    } else {
    	printf("\taverage packet inter-arrival time = %.6g sec\n", 0.0);
    	printf("\taverage packet service time = %.6g sec\n\n", 0.0);
    }
    printf("\taverage number of packets in Q1 = %.6g\n", (float) totalQ1time/emulation_time);
    printf("\taverage number of packets in Q2 = %.6g\n", (float) totalQ2time/emulation_time);
    printf("\taverage number of packets in S1 = %.6g\n", (float) totalServiceTime[0]/emulation_time);
    printf("\taverage number of packets in S2 = %.6g\n\n", (float) totalServiceTime[1]/emulation_time);
    if ( completedPackets ) {
    	printf("\taverage time a packet spent in system = %.6g sec\n", (float) totalSysTime/(completedPackets*1000));
    	printf("\tstandard deviation for time spent in system = %.6g\n\n", stdDev);
    } else {
	printf("\taverage time a packet spent in system = %.6g sec\n", 0.0);
	printf("\tstandard deviation for time spent in system = %.6g\n\n", 0.0);
    }
    ( token_num ) ? printf("\ttoken drop probability = %.6g\n", (float) droppedTokenCount/token_num) : printf("\ttoken drop probability = %.6g\n", 0.0);
    ( packet_count ) ? printf("\tpacket drop probability = %.6g\n", (float) droppedPackets/packet_count) : printf("\tpacket drop probability = %.6g\n", 0.0);
}

static
void readline( FILE *file_handler ) {
    char read_buffer[1024];
	if ( readme && fgets(read_buffer, sizeof(read_buffer), file_handler) != NULL ) {
	    char *start_ptr = read_buffer;
	    char *tab_ptr = strchr(start_ptr, '\t');
	    char *space_ptr = strchr(start_ptr, ' ');

	    if ( tab_ptr != NULL && space_ptr != NULL ) {
	   	if ( tab_ptr < space_ptr ) {
		    *tab_ptr++ = '\0';
		    lambda_inv = atoi(start_ptr);
		    start_ptr = tab_ptr;
	    	} else {
		    *space_ptr++ = '\0';
		    lambda_inv = atoi(start_ptr);
		    start_ptr = space_ptr;
	    	}
	    	while ( *start_ptr == ' ' ) start_ptr++;
	    	while ( *start_ptr == '\t' ) start_ptr++;
	    	tab_ptr = strchr(start_ptr, '\t');
	    	space_ptr = strchr(start_ptr, ' ');
	    	if ( tab_ptr < space_ptr ) {
		    *tab_ptr++ = '\0';
		    P = atoi(start_ptr);
		    start_ptr = tab_ptr;
	    	} else {
		    *space_ptr++ = '\0';
		    P = atoi(start_ptr);
		    start_ptr = space_ptr;
	    	}
	    	while ( *start_ptr == ' ' ) start_ptr++;
	    	while ( *start_ptr == '\t' ) start_ptr++;
	    	tab_ptr = strchr(start_ptr, '\t');
	    	space_ptr = strchr(start_ptr, ' ');
	    	if ( tab_ptr != NULL ) *tab_ptr = '\0';
	    	if ( space_ptr != NULL ) *space_ptr = '\0';
	    	mu_inv = atoi(start_ptr);
	    } else {
		char *delimiter_ptr = malloc(sizeof(read_buffer)), delimiter;
		if ( space_ptr == NULL ) {
		    delimiter_ptr = tab_ptr;
		    delimiter = '\t';
		} else {
		    delimiter_ptr = space_ptr;
		    delimiter = ' ';
		}
		*delimiter_ptr++ = '\0';
		lambda_inv = atoi(start_ptr);
		start_ptr = delimiter_ptr;
		while ( *start_ptr == delimiter ) start_ptr++;
		delimiter_ptr = strchr(start_ptr, delimiter);
		*delimiter_ptr++ = '\0';
		P = atoi(start_ptr);
		start_ptr = delimiter_ptr;
		while ( *start_ptr == delimiter ) start_ptr++;
		delimiter_ptr = strchr(start_ptr, delimiter);
		if ( delimiter_ptr != NULL ) *delimiter_ptr = '\0';
	  	mu_inv = atoi(start_ptr);
	    }
	}
}

/*------------------------------ Arrival Thread -----------------------------*/
static
void *arrival_thread ( void *arg) {
    FILE *file_handler = (FILE *)arg;
    float prev_packet_time = 0;

    while ( packet_count < num ) {
    	//Read from file for lambda, P and service time, if lambda_inv and service_time > 10000ms consider it as 10000ms
	readline(file_handler);
	usleep(lambda_inv*1000);
	packet_count++;
	if ( P > B ) {
	    droppedPackets++;
	    float inter_arrival = nowTime() - prev_packet_time;
	    prev_packet_time = nowTime();
	    totalInterArrivalTime += inter_arrival;
	    Output(nowTime(), 1, packet_count, "drop", "Q1", P, inter_arrival, 0);
	    continue;
	}
	packets *obj = malloc(sizeof(packets));
	obj->start = nowTime();
	float inter_arrival = obj->start - prev_packet_time;
	prev_packet_time = obj->start;
	Output(nowTime(), 1, packet_count, "arrive", "Q1", P, inter_arrival, 0);
	totalInterArrivalTime += inter_arrival;
        obj->number = packet_count;
        obj->num_tokens = P;
	obj->service_time = mu_inv;

	pthread_mutex_lock(&mutex);
	My402ListAppend(&q1_list, (void*)obj);
	obj->arrival = nowTime();
	pthread_mutex_unlock(&mutex);
	Output(nowTime(), 1, packet_count, "enter", "Q1", 0, 0, 0);
	generateQ2();
    }
    pthread_mutex_lock(&mutex);
    q_complete = 1;
    pthread_mutex_unlock(&mutex);
    if (readme) fclose(file_handler);
    pthread_exit((void*)0);
}

/*------------------------------ Token Bucket -----------------------------*/
static
void *token_bucket_thread () {
    while ( 1 ) {
	// Get token and drop it or add it accordingly
	usleep(r_inv*1000);
	pthread_mutex_lock(&mutex);
	if (  tokenCount < B ) {
	    tokenCount++;
	    token_num++;
	    pthread_mutex_unlock(&mutex);
	    Output(nowTime(), 0, token_num, "arrive", "", tokenCount, 0, 0);
	} else {
	    droppedTokenCount++;
	    token_num++;
	    pthread_mutex_unlock(&mutex);
	    Output(nowTime(), 0, token_num, "drop", "", 0, 0, 0);
	}
	generateQ2();
	pthread_mutex_lock(&mutex);
	if ( q_complete == 1 && q1_list.num_members == 0 ) {
	    pthread_mutex_unlock(&mutex);
	    pthread_cancel(CtrlThread);
	    pthread_cond_broadcast(&awake);
	    pthread_exit((void*)0);
	}
	pthread_mutex_unlock(&mutex);
    }
}

/*----------------------------- Server System --------------------------*/
static
void *server(void *arg) {
    char *server_num = (char *) arg;
    while ( 1 ) {
	pthread_mutex_lock(&mutex);
	if ( q_complete == 1 && q1_list.num_members == 0 && q2_list.num_members == 0 ) {
	    pthread_mutex_unlock(&mutex);
	    pthread_cond_signal(&awake);
	    pthread_exit((void *)0);
	}

	// Read first element in Q2
	if ( q2_list.num_members == 0 ) {
	    pthread_cond_wait(&awake, &mutex);
	    if ( q2_list.num_members == 0 ) {
		pthread_mutex_unlock(&mutex);
		pthread_cond_signal(&awake);
		pthread_exit((void *)1);
	    }
	}
	float start = nowTime();
	My402ListElem *firstQ2elem = My402ListFirst(&q2_list);
        framed_packets *firstPacket = (framed_packets *) firstQ2elem->obj;
	My402ListUnlink(&q2_list, firstQ2elem);
	pthread_mutex_unlock(&mutex);

	firstPacket->departure = nowTime();
	int packet_num = firstPacket->packet->number;
	Output(nowTime(), 1, packet_num, "leave", "Q2", 0, (firstPacket->departure - firstPacket->arrival), 0);
	float serviceTime = firstPacket->packet->service_time;
	Output(nowTime(), 1, packet_num, "enter", server_num, 0, serviceTime, 0);
	usleep(serviceTime * 1000);
	float end = nowTime();
	Output(nowTime(), 1, packet_num, "leave", server_num, 0, (end - start), (end - firstPacket->packet->start));

	// Statistics data
	if ( strcmp(server_num, "S1") == 0 ) {
	    totalServiceTime[0] += (end-start);
	} else {
	    totalServiceTime[1] += (end-start);
	}
	pthread_mutex_lock(&mutex);
	totalSysTime += (end - firstPacket->packet->start);
	sqTotalSysTime += ((end - firstPacket->packet->start) * (end - firstPacket->packet->start));
    	totalQ1time += (firstPacket->packet->departure - firstPacket->packet->arrival);
	totalQ2time += (firstPacket->departure - firstPacket->arrival);
	free(firstPacket->packet);
	free(firstPacket);
	if ( sig == 15 ) {
	    pthread_mutex_unlock(&mutex);
	    //Output(nowTime(), 2, 0, "", server_num, 0, 0, 0);
	    pthread_cond_signal(&awake);
	    pthread_exit((void *)1);
	}
	pthread_mutex_unlock(&mutex);
    }
}

/*----------------------------- ctrl+c handler -------------------------*/
static
void *ctrl_c_handler () {
    // Unblock ctrl+c signal and wait for it
    int sig_found = 0;
    sigwait(&set, &sig_found);
    pthread_mutex_lock(&mutex);
    sig = 15;
    pthread_mutex_unlock(&mutex);
    // Once the ctrl comes kill arrival_thread and token_bucket_thread Empty Q1, Q2.
    // After server_transmit are not busy kill them too!!
    pthread_mutex_lock(&output_mutex);
    printf ("\nRecieved ctrl+c signal at %f. Killing threads safely\n", nowTime());
    pthread_mutex_unlock(&output_mutex);
    pthread_cancel(TokenBucketThread);
    pthread_cancel(Q1Thread);
    unlink_lists();
    pthread_cond_signal(&awake);
    pthread_cond_signal(&awake);
    pthread_exit((void*)0);
}

/*-------------------------------- Main --------------------------------*/
int main(int argc, char **argv) {
    // Block all signals
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigprocmask(SIG_BLOCK, &set, 0);

    // Read for lambda, mu, B, P, r and num from commandline arguments if provided.
    // If 1/lambda and 1/mu is greater than 10 sec consider it as 10.
    int file_position = 0;
    float lambda = 1, mu = 0.35, r = 1.5;
    if ( argc % 2 == 1 && argc != 1 ) {
	int i;
	for ( i = 1 ; i < argc; i++ ) {
	    float temp = atof(*(argv+i+1));
	    if ( strcmp(*(argv+i), "-lambda") == 0 ) {
		i++;
		lambda = temp;
		lambda_inv = (float) 1000/temp;
		if ( lambda_inv > 10000 ) lambda_inv = 10000;
	    } else if ( strcmp(*(argv+i), "-mu") == 0 ) {
		i++;
		mu = temp;
		mu_inv = (float) 1000/temp;
		if ( mu_inv > 10000 ) mu_inv = 10000;
	    } else if ( strcmp(*(argv+i), "-r") == 0 ) {
		i++;
		r = temp;
		r_inv = (float) 1000/temp;
		if ( r_inv > 10000 ) r_inv = 10000;
	    } else if ( strcmp(*(argv+i), "-B") == 0) {
		i++;
		B = (int) temp;
	    } else if ( strcmp(*(argv+i), "-P") == 0) {
		i++;
		P = (int) temp;
	    } else if ( strcmp(*(argv+i), "-n") == 0) {
		i++;
		num = (int) temp;
	    } else if ( strcmp(*(argv+i), "-t") == 0) {
		i++;
		readme = 1;
		file_position = i;
	    } else {
		printf ("Wrong argument type %s\nUsage:\twarmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n", *(argv+i));
		exit(0);
	    }
	}
    } else {
	if ( argc != 1 ) {
	    printf("Wrong arguments usage\nUsage:\twarmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
	    exit(0);
	}
    }

    char *filename = *(argv+file_position), read_buffer[15];
    FILE *file_handler = NULL;
    if ( readme ) {
	if ( access(filename, F_OK) == -1 ) {
	    Output(nowTime(), 3, packet_count, "doesn't exist", filename, 0, 0, 0);
	    exit(0);
	}

        struct stat file_check;
        stat(filename, &file_check);
        if ( S_ISDIR(file_check.st_mode) ) {
	    Output(nowTime(), 3, packet_count, "is a directory", filename, 0, 0, 0);
            exit(0);
	}
        if ( access(filename, R_OK) == -1 ) {
	    Output(nowTime(), 3, packet_count, "cannot be read, permission denied", filename, 0, 0, 0);
            exit(0);
        }
        file_handler = fopen(filename, "r");
        if ( file_handler == NULL) {
	    Output(nowTime(), 3, packet_count, "cannot be opened", filename, 0, 0, 0);
            exit(0);
        }
	fgets(read_buffer, sizeof(read_buffer), file_handler);
	char *start_ptr = read_buffer;
	char *space_ptr = strchr(start_ptr, ' ');
	char *tab_ptr = strchr(start_ptr, '\t');
	if ( tab_ptr != NULL ) *tab_ptr = '\0';
	if ( space_ptr != NULL ) *space_ptr = '\0';
	num = atoi(start_ptr);
	if ( num == 0 ) {
	    Output(nowTime(), 3, packet_count, "is not in right format", filename, 0, 0, 0);
	    exit(0);
	}
    }

    printf("Emulation Parameters:\n");
    printf("\tnumber to arrive = %d\n", num);
    if ( !readme ) printf("\tlambda = %f\n",lambda); 
    if ( !readme ) printf("\tmu = %f\n",mu); 
    printf("\tr = %f\n", r);
    printf("\tB = %d\n", B);
    if ( !readme ) printf("\tP = %d\n",P);
    if ( readme ) printf("\ttsfile = %s\n", filename);
    (void)My402ListInit(&q1_list);
    (void)My402ListInit(&q2_list);

    gettimeofday(&time_now, 0);
    start_sec = time_now.tv_sec;
    start_usec = time_now.tv_usec;
    printf ("%011.3fms: emulation begins\n", (float) 0.0);
    pthread_create( &Q1Thread, 0, arrival_thread, (void *) file_handler );
    pthread_create( &TokenBucketThread, 0, token_bucket_thread, NULL);
    pthread_create( &Server1Thread, 0, server, (void *) "S1");
    pthread_create( &Server2Thread, 0, server, (void *) "S2");
    pthread_create( &CtrlThread, 0, ctrl_c_handler, NULL);
    pthread_join( Q1Thread, 0 );
    pthread_join( TokenBucketThread, 0 );
    pthread_join( Server1Thread, 0 );
    pthread_join( Server2Thread, 0 );
    pthread_join( CtrlThread, 0 );
    printf ("%011.3fms: emulation ends\n", nowTime());
    statistics();
    return 0;
}
