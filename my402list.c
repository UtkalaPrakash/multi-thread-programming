#include <stdio.h>
#include <stdlib.h>
#include "my402list.h"
#include "cs402.h"

int My402ListLength ( My402List *myList ) {
    return (myList->num_members);
}

int  My402ListEmpty(My402List *myList) {
    if ( myList->num_members > 0) {
        return (FALSE);
    } else {
        return (TRUE);
    }
}

int My402ListAppend(My402List *myList, void *myObj) {
    return(My402ListInsertBefore(myList, myObj, &(myList->anchor)));
}

int My402ListPrepend(My402List *myList, void *myObj) {
    return(My402ListInsertAfter(myList, myObj, &(myList->anchor)));
}

void My402ListUnlink(My402List *myList, My402ListElem *myElem) {
    myElem->prev->next = myElem->next;
    myElem->next->prev = myElem->prev;
    myList->num_members--;
    free(myElem);
}

void My402ListUnlinkAll(My402List *myList) {
    My402ListElem *myElem = My402ListFirst(myList);
    int i = 0;
    while ( i < myList->num_members ) {
	free(myElem);
        myElem = My402ListNext(myList, myElem);
        i++;
    }
    My402ListInit(myList);
}

int My402ListInsertAfter(My402List *myList, void *newObj, My402ListElem *myElem) {
    My402ListElem *newPtr = malloc(sizeof(My402ListElem));
    newPtr->obj = newObj;
    newPtr->prev = myElem;
    newPtr->next = myElem->next;
    myElem->next->prev = newPtr;
    myElem->next = newPtr;
    myList->num_members++;
    return (TRUE);
}

int My402ListInsertBefore(My402List *myList, void *newObj, My402ListElem *myElem) {
    My402ListElem *newPtr = malloc(sizeof(My402ListElem));
    newPtr->obj = newObj;
    newPtr->prev = myElem->prev;
    newPtr->next = myElem;
    myElem->prev->next = newPtr;
    myElem->prev = newPtr;
    myList->num_members++;
    return(TRUE);
}

My402ListElem *My402ListFirst(My402List *myList) {
    if (myList->num_members > 0 ) {
        return (myList->anchor.next);
    } else {
        return (NULL);
    }
}

My402ListElem *My402ListLast(My402List *myList) {
    if (myList->num_members > 0 ) {
        return (myList->anchor.prev);
    } else {
        return (NULL);
    }
}

My402ListElem *My402ListNext(My402List *myList, My402ListElem *myElem) {
    if ( myElem->next == &(myList->anchor) ) {
        return(NULL);
    } else {
        return(myElem->next);
    }
}

My402ListElem *My402ListPrev(My402List *myList, My402ListElem *myElem) {
    if ( myElem->prev == &(myList->anchor) ) {
        return(NULL);
    } else {
        return(myElem->prev);
    }
}

My402ListElem *My402ListFind(My402List *myList, void *myObj) {
    My402ListElem *myElem = My402ListFirst(myList);
    int i = 0;
    while ( i < myList->num_members ) {
        if ( myElem->obj == myObj ) return(myElem);
        myElem = My402ListNext(myList, myElem);
        i++;
    }
    return (NULL);
}

int My402ListInit(My402List *myList) {
    (myList->anchor).next = &(myList->anchor);
    (myList->anchor).prev = &(myList->anchor);
    myList->num_members = 0;
    return (TRUE);
}
